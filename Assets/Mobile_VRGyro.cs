﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mobile_VRGyro : MonoBehaviour {

    void Start()
    {
        if (Input.isGyroAvailable)
        {
            Input.gyro.enabled = true;
        }
    }

    void Update () {
        Quaternion rot = Input.gyro.attitude;
        transform.localRotation = new Quaternion(rot.x, rot.y, -rot.z, -rot.w);
	}
}
