﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mobile_VRMovement : MonoBehaviour {

    public Transform lookPosition;
    bool toggle;
    bool walkEnabled;

    void Update () {
        if(walkEnabled){
            Vector3 temp = (lookPosition.position - transform.position);
            transform.position += new Vector3(temp.x, 0, temp.z) / 100;
        }

        if (lookPosition.position.y < 1.25)
        {
            if (toggle)
            {
                if (walkEnabled)
                {
                    walkEnabled = false;
                    Debug.Log("disabling walk-mode!");
                }
                else
                {
                    walkEnabled = true;
                    Debug.Log("enabling walk-mode!");
                }
                toggle = false;
            }
        }
        else
        {
            toggle = true;
        }
    }
}
